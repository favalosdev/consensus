# Consensus

Consensus is a project for a voting system, via a discord bot.



## How it works

## Installation

Clone the project

`git clone https://gitlab.com/araly/consensus.git`

`cd consensus/`

Initialize a NodeJS project

`npm init` Leave the fields as their defaults

Install the [discord.js](https://discord.js.org/) module

`npm install discord.js`

Replace the TOKEN string in index.js by your bot's token.

## Usage

To run the bot on your computer

`node .` while in the project's directory